  jenkins-jnlp-slave:
    image: ${image_type}
    environment:
      - JENKINS_URL=${jenkins_url}
      - JENKINS_SECRET=${jenkins_secret}
      - JENKINS_AGENT_NAME=${jenkins_agent_name}
    {{- if eq .Values.enable_docker_inside "true"}}
    volumes:
      - ${docker_sock_path}:/var/run/docker.sock:ro
    {{- end }}